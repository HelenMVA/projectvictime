const express = require('express');
const api = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const model = require('./model/comere');
const auth = require('../auth/routes');

api.use(bodyParser.urlencoded({ extended: true }));
api.use(bodyParser.json());
api.use(cors());



// Connection à la base de donnée
mongoose.connect('mongodb://localhost:27017/DashBoard', { useNewUrlParser: true }, () => {
    console.log('On est connecte')
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Erreur lors de la connexion"));
db.once("open", () => {
    console.log("Connexion à la DB");
});


/* Пост данных стукача после регистрации */
const newComere = mongoose.model('NewComere', new mongoose.Schema(
    {
        nom: String,
        prenom: String,
        email: String,
        pass: String
    }));
    
api.post("/newcomere", (req, res) => {
    let comereData = req.body;
    let valueComere = new newComere(comereData);

    console.log(valueComere);

    valueComere.save((err,comere) => {
        if (err) {
            // res.send(err); заменить на return  более правильно 
            console.log('ошибка пост регистрация стукача');
            return res.status(500).json(err);
        }
        // res.send();
        console.log('стукач зареган');
        res.status(201).json(comere);
    })
});

/* Для connect  Commere */
// var newConnect = mongoose.model('connectComere', new mongoose.Schema(
//     {
//         nom: String,
//         pass: String
//     }));

api.post("/connexion", (req, res) => {
    console.log('§§§');
    let infoData = req.body;
    newComere.findOne({ nom: infoData.nom, pass: infoData.pass }, (error, commere) => {
        console.log('commere essaye dentrer ');
        if (commere === null){
            console.log('commere nexiste pas dans db ');
            res.send();
        } else if (commere.nom === infoData.nom && commere.pass === infoData.pass){
            console.log('commere est entre ');
            res.send();
        } else {
            console.log('commere s est tromper avec son pass');;
        }
    });
});


/* Пост данных жервы после регистрации */
const newVictime = mongoose.model('NewVictime', new mongoose.Schema(
    {
        nom: String,
        prenom: String,
        email: String,
        pass: String
    })
    );
api.post("/newvictime", (req, res) => {
    let victimeData = req.body;
    let valueVictime = new newVictime(victimeData);

    console.log(valueVictime);

    valueVictime.save((err, victime) => {
        if (err) {
            // res.send(err); заменить на return  более правильно 
            console.log('ошибка пост регистрация жертва');
            return res.status(500).json(err);
        }
        // res.send();
        console.log('жертва зарегана');
        res.status(201).json(victime);
    })
});


/* Пост данных оценок из личного пространства стукача */

const newInfo = mongoose.model('NewInfo', new mongoose.Schema(
    {
        nom: String,
        prenom: String,
        date: String,
        note: String,
        abscence: String,
        comment: String
    }));
api.post("/newinfo", (req, res) => {
    let infoData = req.body;
    let valueInfo = new newInfo(infoData);

    console.log(valueInfo);

    valueInfo.save((err, info) => {
        if (err) {
            // res.send(err); заменить на return  более правильно 
            console.log('ошибка пост доноса');
            return res.status(500).json(err);
        }
        // res.send();
        console.log('донос отправлен');
        res.status(201).json(info);
    })
});

/* Get pour la page espace personelle  comere*/
api.get('/espacecomere', (req, res) => {
    console.log('fait le demande pour espace personelle Commere')
    newInfo.find({},
        (err, victime) => {
            if (err) {
                res.status(500).send("Pas possible d'afficher les utilisateurs");
            }
            res.json(victime);
            console.log('on a recupere nos victimes')
            // res.send(user);
        });
});



api.listen(8080, () => {
    console.log('сервер API')
})