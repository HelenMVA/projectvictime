import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { victime } from './models/victime';

@Injectable({
  providedIn: 'root'
})
export class VictimeService {
  APIadress = 'http://localhost:8080/'

  constructor(private httpClient: HttpClient) { }



/* post for victime */
  createVictime(victimePost: victime) {
    console.log('постим нашу жертву');
    console.log(victimePost);
    return this.httpClient.post<victime>(`${this.APIadress}newvictime`, victimePost);
  }

}
