import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { comere } from './models/comere';

@Injectable({
  providedIn: 'root'
})
export class ComereService {

  APIadress = 'http://localhost:8080/'

  constructor(private httpClient: HttpClient) { }

  // getUsers(): Observable<Users[]> {
  //   return this.httpClient.get<comere[]>(`${this.APIadress}getUsers`);
  // }

  /* post for comere */
  createComere(comerePost: comere) {
    console.log('постим нашего стукача');
    console.log(comerePost);
    return this.httpClient.post<comere>(`${this.APIadress}newcomere`, comerePost);
  };
  connectComere(comconnectPost: comere) {
    console.log('стукач зашел на сайт');
    console.log(comconnectPost);
    return this.httpClient.post<comere>(`${this.APIadress}connexion`, comconnectPost);
  }

}
