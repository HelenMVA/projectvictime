import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { info } from './models/info';

@Injectable({
  providedIn: 'root'
})
export class InfoService {

  APIadress = 'http://localhost:8080/'

  constructor(private httpClient: HttpClient) { }

    getAllvictimes(): Observable<info[]> {
      return this.httpClient.get<info[]>(`${this.APIadress}espacecomere`);   
  }

  /* post for   info comere */
  createInfo(infoPost: info) {
    console.log('постим нащ  донос');
    console.log(infoPost);
    return this.httpClient.post<info>(`${this.APIadress}newinfo`, infoPost);
  }

}
