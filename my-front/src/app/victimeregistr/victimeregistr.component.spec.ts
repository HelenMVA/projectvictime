import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VictimeregistrComponent } from './victimeregistr.component';

describe('VictimeregistrComponent', () => {
  let component: VictimeregistrComponent;
  let fixture: ComponentFixture<VictimeregistrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VictimeregistrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VictimeregistrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
