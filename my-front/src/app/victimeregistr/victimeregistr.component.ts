import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms'; /* moi */
import { VictimeService } from '../victime.service';/* moi */
import { Router } from '@angular/router'; /* для доррог перенаправления */

@Component({
  selector: 'app-victimeregistr',
  templateUrl: './victimeregistr.component.html',
  styleUrls: ['./victimeregistr.component.scss']
})
export class VictimeregistrComponent implements OnInit {
  creationForm: FormGroup;   /* типа переменная создается */

  constructor(private fb: FormBuilder, private victimeService: VictimeService, private router: Router) { }

  ngOnInit() {
    this.createFormForVictime();
  }
  createFormForVictime() {
    this.creationForm = this.fb.group({
      nom: '',
      prenom: '',
      email: '',
      pass: ''
    });
  }
  createVictimeNew() {  /* имя функции как в html файле для формы */
    if (this.creationForm.valid) {
      console.log('моя жертва валябельна ', this.creationForm);
        this.victimeService
        .createVictime(this.creationForm.value)
        .subscribe(data => {
          console.log(data, 'мы создали нашу жертву');
          this.router.navigate(['/']); 
        })
    }
  }

}
