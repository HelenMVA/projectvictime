import { Component, OnInit } from '@angular/core';
import { trigger, state,style } from '@angular/animations';
import { PipeCollector } from '@angular/compiler/src/template_parser/binding_parser';

@Component({
  selector: 'app-animation',
  templateUrl: './animation.component.html',
  styleUrls: ['./animation.component.scss'],
  // animations: [
  //   trigger('urss',[
  //     state('start',style({
  //       backgroundColor: 'green',
  //       width: '150px',
  //       height: '150px',
  //     })),
  //     state('end', style({
  //       backgroundColor: 'red',
  //       width: '300px',
  //       height: '300px',
  //     }))
  //   ])
  // ]
})
export class AnimationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
