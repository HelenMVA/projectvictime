import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComereregistrComponent } from './comereregistr.component';

describe('ComereregistrComponent', () => {
  let component: ComereregistrComponent;
  let fixture: ComponentFixture<ComereregistrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComereregistrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComereregistrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
