import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms'; /* moi */
import { ComereService } from '../comere.service';/* moi */
import { Router } from '@angular/router';


@Component({
  selector: 'app-comereregistr',
  templateUrl: './comereregistr.component.html',
  styleUrls: ['./comereregistr.component.scss']
})
export class ComereregistrComponent implements OnInit {
  creationForm: FormGroup; /* типа переменная создается */

  constructor(private fb: FormBuilder, private comereService: ComereService, private router: Router) { }

  ngOnInit() {    /* для формы коннект и регистрации */
    this.createFormForComere();
    this.createFormForComConnect();
  }
  createFormForComere() {  /* для формы коннект и регистрации */
    this.creationForm = this.fb.group({
      nom: '',
      prenom: '',
      email: '',
      pass: ''
    });
  }
  createFormForComConnect() {  /* для формы коннект и регистрации */
    this.creationForm = this.fb.group({
      nom: '',
      pass:''
    });
  }
  createComereNew(){  /* имя функции как в html файле для формы */
    if (this.creationForm.valid){
      console.log('мой стукач валябелен ', this.creationForm);
      this.comereService
      .createComere(this.creationForm.value)
      .subscribe(data=>{
        console.log(data, 'мы создали нашего стукача');
        this.router.navigate(['/espacecomere']); /* для перенаправления на др стр */
      })
    }
  };
  connectComere() {  /* имя функции как в html файле для формы */
    if (this.creationForm.valid) {
      console.log('мой стукач зашел на сайт ', this.creationForm);
      this.comereService
        .connectComere(this.creationForm.value)
        .subscribe(data => {
          console.log(data, 'он зашел');
          this.router.navigate(['/espacecomere']); /* для перенаправления на др стр */
        })
    }
  };

}
