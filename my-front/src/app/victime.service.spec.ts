import { TestBed } from '@angular/core/testing';

import { VictimeService } from './victime.service';

describe('VictimeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VictimeService = TestBed.get(VictimeService);
    expect(service).toBeTruthy();
  });
});
