import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'  /* для соединения с api serverom */
import { ReactiveFormsModule, FormsModule } from '@angular/forms'; /* moi */
import { RouterModule } from '@angular/router'; /* для страниц дороги */

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComereregistrComponent } from './comereregistr/comereregistr.component';
import { VictimeregistrComponent } from './victimeregistr/victimeregistr.component';
import { ComereEspacePersComponent } from './comere-espace-pers/comere-espace-pers.component';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';  /* материал ангуляра после установки добавился */
import { MaterialModule} from './material.module'; /* добавим этот модуль который создали сами что бы рабоать с материалио , */
import { AnimationComponent } from './animation/animation.component';


const routes = [
  { path: '', component: HomeComponent },
  { path: 'newcomere', component: ComereregistrComponent },
  { path: 'newvictime', component: VictimeregistrComponent },
  { path: 'espacecomere', component: ComereEspacePersComponent },
  { path: 'animation', component: AnimationComponent },
  { path: 'espacevictime', component: AnimationComponent },
]; 

@NgModule({
  declarations: [
    AppComponent,
    ComereregistrComponent,
    VictimeregistrComponent,
    ComereEspacePersComponent,
    HomeComponent,
    AnimationComponent
    
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes), /* для дорог добавить */
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    MaterialModule,
    

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
