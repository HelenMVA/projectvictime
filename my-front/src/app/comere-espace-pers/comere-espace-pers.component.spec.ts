import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComereEspacePersComponent } from './comere-espace-pers.component';

describe('ComereEspacePersComponent', () => {
  let component: ComereEspacePersComponent;
  let fixture: ComponentFixture<ComereEspacePersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComereEspacePersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComereEspacePersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
