import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms'; /* moi */
import { InfoService } from '../info.service';/* moi */
import { Observable } from 'rxjs'; /* что бы забрать список учеников жертв */
import { info } from '../models/info';

@Component({
  selector: 'app-comere-espace-pers',
  templateUrl: './comere-espace-pers.component.html',
  styleUrls: ['./comere-espace-pers.component.scss']
})
export class ComereEspacePersComponent implements OnInit {
  user = { nom: "" } ;
  creationForm: FormGroup; /* типа переменная создается */
  victimeGetList$: Observable<info[]>   /* что бы забрать жертв */

  constructor(private fb: FormBuilder, private infoService: InfoService, private GetAllvictimes: InfoService ) { }

  ngOnInit() {
    this.createFormForInfo(); /*  будет обновляться каждый раз при загрузке стр  ??????*/
    this.victimeGetList$ = this.GetAllvictimes.getAllvictimes();/* что бы забрать жертв такая же как в форме htmp*/
  }
  createFormForInfo() {
    this.creationForm = this.fb.group({

      nom: '',
      prenom: '',
      date: '',
      note: '',
      abscence: '',
      comment: ''
    });
  }

  createInfoNew() {  /* имя функции как в html файле для формы */
    if (this.creationForm.valid) {
      console.log('донос валябелен', this.creationForm);
      this.infoService
        .createInfo(this.creationForm.value)
        .subscribe(data => {
          console.log(data, 'мы отправили донос');
        })
    }
  }

}
